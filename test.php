<?php

try {
	$dsn = "mysql:host=localhost;dbname=tunombrebbdd";
	$db = new PDO($dsn, "root");
	$options = array(
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );
} catch (PDOException $e){
	echo $e->getMessage();
}

$host = "host";
$usuario = "usuario";
$password = "password";

//OPCIÓN RÁPIDA QUE NO CONTEMPLA LA PORTABILIDAD SI QUEREMOS EXTRAER LA CUENTA POR rowCount()
/*$sql = "SELECT * FROM client_config WHERE host = :host";
$stmt = $db->prepare($sql);
$stmt->bindParam(":host",$host);
$stmt->execute();

echo $stmt->rowCount();

while($row = $stmt->fetch(PDO::FETCH_OBJ)){
    echo "Nombre: " . $row->usuario . "<br>";
    echo "Nav: " . $row->direccion_nav . "<br>";
	echo "Empresa: " . $row->direccion_empresa . "<br>";
}*/

//OPCIÓN QUE Sí CONTEMPLA LA PORTABILIDAD DE LA EXTRACCIÓN DE LA CUENTA DE FILAS, SE EMPLEAN DOS CONSULTAS
$sql = "SELECT COUNT(*) FROM client_config WHERE host = :host";
$stmt = $db->prepare($sql);
$stmt->bindParam(":host",$host);
$stmt->execute();

$count = $stmt->fetchColumn();
echo $count . "<br>";
echo $stmt->errorCode() . "<br>";

//TRAS SACAR LA CUENTA CREAMOS UNA NUEVA CONSULTA QUE NOS MUESTRA LOS DATOS
if ($stmt->errorCode() == 0){
	
	if ($count > 0) {
		$sql = "SELECT * FROM client_config WHERE host = :host";
		$stmtRows = $db->prepare($sql);
		$stmtRows->bindParam(":host",$host);
		$stmtRows->execute();
		if ($stmtRows->errorCode() == 0){
			while ($client = $stmtRows->fetch(PDO::FETCH_OBJ)){
				echo "Nombre: " .  $client->usuario . "<br>";
				echo "Nav: " . $client->direccion_nav . "<br>";
				echo "Empresa: " . $client->direccion_empresa . "<br>";
			}
		}
		/*foreach ($stmtRows->fetch(PDO::FETCH_OBJ) as $fila) {
			   echo "Nombre: " .  $fila->usuario . "<br>";
			   echo "Nav: " . $fila->direccion_nav . "<br>";
		}*/
	}else{
		echo "No hay registros";
	}
}

$db = null;
?>